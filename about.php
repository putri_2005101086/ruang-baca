<!DOCTYPE html>
<html>
<head>
	<title>About Us</title>
	<link rel="stylesheet" type="text/css" href="about.css">

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Josepfin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

</head>
<body>
	<section class="about">
		<div class="main">
			<img src="images/uli.jpg">
			<div class="about-text">
				<h1>About</h1>
				<h5>Ruang<span> Baca</span></h5>
				<p>Ruang Baca adalah website yang memungkinkan penggunanya untuk membaca sebuah buku secara gratis!
				Membaca itu  bukan insting tetapi keterampilan yang harus diasah,jadi investasikan diri dengan membaca buku.
				Website ini dibuat oleh Putri Wulandari Mahasiswa Universitas PGRI Madiun Prodi Teknik Informatika sebagai Tugas dari mata kuliah Pemograman Web</p>
			</div>
		</div>
	</section>
</body>
</html>